#include <iostream>
#include <cblas.h>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>

#include "gaussian.hh"
#include "math_kernel/lapacke/lapacke.h"
#include "math_kernel/tools/tools.hh"

#define EIGEN_THRESHOLD 1.0

void Gaussian::init(unsigned int dim_)
{
  diagonal = true;
  dim = dim_;
  mean = new double[dim];
  cov = new double[dim * dim];
  cov_inv = new double[dim * dim];

  for (size_t k = 0; k < dim; ++k)
    mean[k] = rand() % (dim * 10);
  for (size_t x = 0; x < dim; ++x)
    for (size_t y = 0; y < dim; ++y)
    {
      cov[x + y * dim] = (x == y);
      cov_inv[x + y * dim] = (x == y);
    }
  compute_params();
}

Gaussian::Gaussian()
{
  cov_inv = 0;
  mean = 0;
  dim = 0;
  cov = 0;
}
Gaussian::Gaussian(unsigned int dim_)
{
  init(dim_);
}


Gaussian::~Gaussian()
{
  if (cov_inv)
    delete[] cov_inv;
  if (cov)
    delete[] cov;
  if (mean)
    delete[] mean;
}

void Gaussian::compute_cov_inv()
{
  inv_sym(cov, dim, cov_inv);
}

void Gaussian::compute_const()
{
  double det = det_sym(cov, dim);

  log_const = log(1.0 / pow((2 * M_PI), (double)dim / 2.0));
  log_const += log(1.0 / sqrt(det));
}

void Gaussian::cov_threshold()
{
  int n = dim;
  int info = 0;
  char uplo = 'U';
  double covtmp[dim * dim];
  double d[dim * dim];
  double tau[dim * dim];
  double e[dim * dim];
  double* work = 0;
  int lwork = -1;
  double wkopt;

  memcpy(covtmp, cov, dim * dim * sizeof(double));
  dsytrd_(&uplo, &n, cov, &n, d, e, tau, &wkopt, &lwork, &info);
  lwork = (int)wkopt;
  work = new double[lwork];
  dsytrd_(&uplo, &n, cov, &n, d, e, tau, work, &lwork, &info);

  //for (unsigned int k = 0; k < dim; ++k)
  //  std::cout << d[k] << std::endl;
}


void Gaussian::cov_diag()
{
  for (size_t y = 0; y < dim; ++y)
    for (size_t x = 0; x < dim; ++x)
    {
      if (x != y)
        cov[x + y * dim] = 0;
      else if (cov[x + y * dim] < EIGEN_THRESHOLD)
        cov[x + y * dim] = EIGEN_THRESHOLD;
    }

  //print_matrix(dim, dim, cov);
}

void Gaussian::compute_params()
{
  //cov_threshold();
  cov_diag();
  compute_cov_inv();
  compute_const();
}

inline double Gaussian::raw_log_prob(double* data)
{
  double res = 0.0;
  double centered_data[dim];
  double lhand[dim];

  cblas_dcopy(dim, data, 1, centered_data, 1);
  cblas_daxpy(dim, -1.0, mean, 1, centered_data, 1);
   if (diagonal)
    {
      for (unsigned int l = 0; l < dim; ++l)
	res += centered_data[l] * centered_data[l] * cov[l + l * dim];
    }
  else
    {
      for (size_t k = 0; k < dim; ++k)
	lhand[k] = 0;

      cblas_dgemv(CblasColMajor, CblasNoTrans, dim, dim,
		  1.0, cov_inv, dim, centered_data, 1,
		  1.0, lhand, 1);
      res = cblas_ddot(dim, centered_data, 1,
		       lhand, 1);
    }
  return -0.5 * res;
}

double Gaussian::prob(double* data)
{
  return exp(log_prob(data));
}

double Gaussian::log_prob(double* data)
{
  double res = 0.0;

  res = log_const + raw_log_prob(data);
  return res;
}

double Gaussian::log_likelihood(double** data, unsigned int n)
{
  double ll = n * log_const;
  double raw_log;

#pragma omp parallel for reduction(+:ll) private(raw_log)
  for (size_t k = 0; k < n; ++k)
  {
    raw_log = raw_log_prob(data[k]);
    ll = ll + raw_log;
  }

  return ll;
}

double Gaussian::general_variance()
{
  double ret = 1.0;

  for (unsigned int x = 0; x < dim; ++x)
    for (unsigned int y = 0; y < dim; ++y)
      if (x == y)
	ret *= cov[x + y * dim];
  return ret;
}

double Gaussian::dist(double* data)
{
  double ret = 0.0;

  for (unsigned int d = 0; d < dim; ++d)
    ret += (data[d] - mean[d]) * (data[d] - mean[d]);
  return ret;
}

double Gaussian::likelihood(double** data, unsigned int n)
{
  return exp(log_likelihood(data, n));
}

void Gaussian::print()
{
  std::cout << "Mean:" << std::endl;
  for (unsigned int k = 0; k < dim; ++k)
    std::cout << mean[k] << " ";
  std::cout << std::endl << std::endl;
  std::cout << "Covariance: " << std::endl;
  print_matrix(dim, dim, cov);
}

void Gaussian::learn(double** data, unsigned int n)
{
  double tmpmean;

  for (unsigned int k = 0; k < dim; ++k)
  {
    tmpmean = 0.0;
#pragma omp parallel for reduction(+:tmpmean) private(data)
    for (unsigned int i = 0; i < n; ++i)
    {
      tmpmean = tmpmean + data[i][k];
    }
    mean[k] = tmpmean;
  }
  for (unsigned int k = 0; k < dim; ++k)
    mean[k] /= n;

  for (unsigned int k = 0; k < dim * dim; ++k)
    cov[k] = 0.0;

  for (unsigned int i = 0; i < n; ++i)
  {
    double centered_data[dim];

    cblas_dcopy(dim, data[i], 1, centered_data, 1);
    cblas_daxpy(dim, -1.0, mean, 1, centered_data, 1);
    cblas_dsyr(CblasColMajor, CblasUpper, dim, 1.0, centered_data, 1,
	       cov, dim);
  }
  for (unsigned int k = 0; k < dim * dim; ++k)
    cov[k] /= n;
  compute_params();
}
