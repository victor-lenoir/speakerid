#ifndef GAUSSIAN_HH_
# define GAUSSIAN_HH_

class Gaussian
{
public:
  Gaussian (unsigned int dim_);
  Gaussian ();
  ~Gaussian();

  void init(unsigned int dim_);
  void print();

  void learn(double** data, unsigned int n);

  double log_prob(double* data);
  double prob(double* data);

  double dist(double* data);

  double log_likelihood(double** data, unsigned int n);
  double likelihood(double** data, unsigned int n);
  
  void compute_params();
  double general_variance();
  unsigned int dim;
  double* mean;
  double* cov;
  bool diagonal;
private:
  void cov_diag();
  void cov_threshold();
  void compute_cov_inv();
  void compute_const();
  double raw_log_prob(double* data);

  double log_const;
  double cov_det;
  double* cov_inv;
};

#endif // !GAUSSIAN_HH_
