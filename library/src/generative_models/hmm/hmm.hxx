#include <iomanip>

template <typename T>
HMM<T>::HMM (double** transition_prob_,
             size_t size_) :
  size (size_),
  transition_prob (transition_prob_)
{
}


template <typename T>
double HMM<T>::get_emission_prob (size_t state, T obs)
{
  return func[state](obs);
}

template <typename T>
std::vector<size_t> HMM<T>::viterbi (std::list<T>& observations)
{
  std::vector<T> v (observations.size());
  size_t k = 0;

  for (typename std::list<T>::iterator it = observations.begin ();
       it != observations.end ();
       it++)
    v[k++] = (*it);
  return viterbi(v);
}

      // Normali
template <typename T>
std::vector<size_t> HMM<T>::viterbi (std::vector<T>& observations)
{
  std::vector<size_t> path;
  double V[size][2];
  double max = 0.0;
  size_t maxstate = 0;

  for (size_t netat = 0; netat < size; ++netat)
  {
    V[netat][0] = get_emission_prob(netat, observations[0]);
    if (V[netat][0] > max)
    {
      max = V[netat][0];
      maxstate = netat;
    }
  }
  path.push_back (maxstate);
  for (size_t t = 1; t < observations.size(); ++t)
  {
    max = 0.0;
    maxstate = 0;

    for (size_t y = 0; y < size; ++y)
    {
      double prob = 0.0;
      double maxou = 0.0;

      // Normalize
      for (size_t toto = 0; toto < size;
           ++toto)
      {
        if (V[toto][0] > maxou)
          maxou = V[toto][0];
      }
      for (size_t toto = 0; toto < size; ++toto)
        V[toto][0] *= (1.0 / maxou);

      for (size_t y0 = 0; y0 < size; ++y0)
      {
        double tmp = V[y0][0] * get_transition_prob (y0, y) *
          get_emission_prob (y, observations[t]);
        if (tmp > prob)
          prob = tmp;
      }
      V[y][1] = prob;
      if (prob > max)
      {
        max = prob;
        maxstate = y;
      }
    }

    for (size_t toto = 0; toto < size; ++toto)
      V[toto][0] = V[toto][1];
    path.push_back (maxstate);
  }
  return path;
}

template <typename T>
double HMM<T>::get_transition_prob (size_t state, size_t state2)
{
  return transition_prob[state][state2];
}
