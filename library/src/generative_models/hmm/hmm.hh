#ifndef HMM_HH_
# define HMM_HH_

# include <vector>
# include <list>
# include <map>
# include <string>
# include <iostream>

template <typename T>
class HMM
{
public:
  typedef double (*Handler)(T);

  /*
  ** HMM constructor
  ** transition_prob_: square matrix of transition probabilities
  ** size_: dimension of the square matrix
  */
  HMM (double** transition_prob_,
       size_t size_);

  void set_handler (std::vector<Handler> func_)
    {
      func = func_;
    }

  std::vector<size_t> viterbi (std::list<T>& observations);
  std::vector<size_t> viterbi (std::vector<T>& observations);

  double get_transition_prob (size_t state,
                              size_t state2);

  virtual double get_emission_prob (size_t state,
                                    T obs);

  size_t size;
  double** transition_prob;
  std::vector<Handler> func;
};

# include "hmm.hxx"

#endif
