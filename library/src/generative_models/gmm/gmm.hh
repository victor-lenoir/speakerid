#ifndef GMM_HH_
# define GMM_HH_

# include "generative_models/gaussian/gaussian.hh"

class GMM
{
public:
  GMM(unsigned int dim_,
      unsigned int nb_gauss_);
  ~GMM();
  void learn(double** data, unsigned int n);
  void free_prob();

  double prob(double* data);
  double log_prob(double* data);

  double log_likelihood(double** data, unsigned int n);
  double likelihood(double** data, unsigned int n);

  double bic(double** data, unsigned int n);
  int nb_params();

  void print();

  double general_variance();
  unsigned int nb_gauss;
  double* w;
  Gaussian* gaussians;
  int dim;
private:
  void init_centers(double** data, unsigned int n);
  void split_centers(unsigned int currnbgauss);
  void update_centers(double** data, unsigned int n,
		      unsigned int currnbgauss);
  void expectation(double** data, unsigned int n);
  void maximization(double** data, unsigned int n);
  double** p; // p[i][t] = p(i|xt)
};


int find_number_components(double** data, unsigned int n,
			   int dim);
#endif // !GMM_HH_
