#include <iostream>
#include <cmath>
#include <cfloat>
#include <cblas.h>
#include <cstring>
#include <omp.h>
#include "math_kernel/lapacke/lapacke.h"
#include "gmm.hh"

#define MINUS_LOG_THRESHOLD     -39.14

int find_number_components(double** data, unsigned int n,
                           int dim)
{
  int optimal = 1;
  int current = 1;
  double minbic = -1;
  double curbic = 0;

  while (1)
  {
    GMM a(dim, current);

    curbic = a.bic(data, n);
    /*
    std::cout << current
              << "-components BIC: "
              << curbic << std::endl;
    */
    if ((minbic < 0) || (curbic < minbic))
    {
      minbic = curbic;
      optimal = current;
    }
    else
      break;
    current *= 2;
    //++current;
  }
  return optimal;
}

void GMM::print()
{
  for (unsigned int k = 0; k < nb_gauss; ++k)
  {
    std::cout << "Gaussian " << k << ":" <<std::endl;
    gaussians[k].print();
    std::cout << "\n\n============================\n\n";
  }
}

inline double logAdd(double log_a,
                     double log_b)
{
  if (log_a < log_b)
    std::swap (log_a, log_b);

  double minusdif = log_b - log_a;
  if (minusdif < MINUS_LOG_THRESHOLD)
    return log_a;
  else
    return log_a + log1p(exp(minusdif));
}

GMM::GMM(unsigned int dim_,
         unsigned int nb_gauss_)
{
  dim = dim_;
  nb_gauss = nb_gauss_;
  gaussians = new Gaussian[nb_gauss_];
  w = new double[nb_gauss_];
  for (unsigned int k = 0; k < nb_gauss_; ++k)
  {
    gaussians[k].init(dim_);
    w[k] = 1.0 / (double)nb_gauss_;
  }
  p = 0;
}

int GMM::nb_params()
{
  return (nb_gauss + nb_gauss * (dim + dim * dim));
}

void GMM::free_prob()
{
  if (p)
  {
    for (unsigned int t = 0; t < nb_gauss; ++t)
      delete[] p[t];
    delete[] p;
  }
}

GMM::~GMM()
{
  delete[] gaussians;
  delete[] w;
  free_prob();
}


double GMM::general_variance()
{
  double ret = 1.0;
  for (unsigned int k = 0;
       k < nb_gauss;
       ++k)
    ret += gaussians[k].general_variance();
  return ret;
}

double GMM::bic(double** data, unsigned int n)
{
  learn(data, n);
  return (-2 * log_likelihood(data, n) +
	  nb_params() * log(n));
}

void GMM::split_centers(unsigned int currnbgauss)
{
  double mean[dim];

  for (unsigned int k = 0; k < currnbgauss; ++k)
  {
    for (int d = 0; d < dim; ++d)
    {
      mean[d] = gaussians[k].mean[d];
      gaussians[k].mean[d] = mean[d] + 1.0;
      gaussians[currnbgauss + k].mean[d] = mean[d] - 1.0;
    }
  }
}

void GMM::init_centers(double** data, unsigned int n)
{
  unsigned int currnbgauss = 1;

  update_centers(data, n, currnbgauss);
  for (int k = 0; k < (int)log2(nb_gauss); ++k)
  {
    split_centers(currnbgauss);
    currnbgauss *= 2;
    update_centers(data, n, currnbgauss);
  }
  for (unsigned int k = 0; k < 10; ++k)
    update_centers(data, n, currnbgauss);
}

void GMM::update_centers(double** data, unsigned int n,
                         unsigned int currnbgauss)
{
  double new_means[currnbgauss][dim];
  int count[currnbgauss];

  for (unsigned int k = 0; k < currnbgauss; ++k)
  {
    for (int d = 0;  d < dim; ++d)
      new_means[k][d] = 0;
    count[k] = 0;
  }
  for (unsigned int t = 0;  t < n; ++t)
  {
    int select = 0;
    double max_prob = 0.0;

    for (unsigned int k = 0; k < currnbgauss; ++k)
    {
      double p = gaussians[k].dist(data[t]);

      if (p > max_prob)
      {
        max_prob = p;
        select = k;
      }
    }

    ++count[select];
    for (int d = 0;  d < dim; ++d)
      new_means[select][d] += data[t][d];
  }
  for (unsigned int k = 0; k < currnbgauss; ++k)
    if (count[k] > 0)
      for (int d = 0;  d < dim; ++d)
      {
        new_means[k][d] /= count[k];
        gaussians[k].mean[d] = new_means[k][d];
      }
}

void GMM::learn(double** data, unsigned int n)
{
  if (nb_gauss == 1)
    gaussians[0].learn(data, n);
  else
  {
    init_centers(data, n);
    for (int iter = 0; iter < 1; ++iter)
    {
      //std::cout << "Iteration " << iter << ":" << std::endl;
      expectation(data, n);
      maximization(data, n);
      //std::cout << std::endl;
    }
    //print();
  }
}

void GMM::maximization(double** data, unsigned int n)
{
  double sum = 0.0;
  double centered_data[dim];

  for (unsigned int gauss = 0; gauss < nb_gauss; ++gauss)
  {
    sum = -DBL_MAX;
    for (unsigned int t = 0; t < n; ++t)
      sum = logAdd(sum, p[gauss][t]);
    // WEIGHT
    w[gauss] = exp(sum - log(n));

    // MEAN
    for (int d = 0; d < dim; ++d)
      gaussians[gauss].mean[d] = 0;
    for (unsigned int t = 0; t < n; ++t)
      for (int d = 0; d < dim; ++d)
      {
        int s = -(data[t][d] < 0) + (data[t][d] > 0);
        gaussians[gauss].mean[d] += s*exp(log(fabs(data[t][d])) +
                                          p[gauss][t]);
      }
    for (int d = 0; d < dim; ++d)
      gaussians[gauss].mean[d] /= exp(sum);

    // COVARIANCE
    for (int k = 0; k < dim * dim; ++k)
      gaussians[gauss].cov[k] = 0.0;
    for (unsigned int t = 0; t < n; ++t)
    {
      cblas_dcopy(dim, data[t], 1, centered_data, 1);
      cblas_daxpy(dim, -1.0, gaussians[gauss].mean, 1, centered_data, 1);
      cblas_dger(CblasColMajor, dim, dim, exp((p[gauss][t] - sum)),
                 centered_data, 1,
                 centered_data, 1, gaussians[gauss].cov, dim);
    }

    gaussians[gauss].compute_params();
  }
}

double GMM::prob(double* data)
{
  return exp(log_prob(data));
}

double GMM::log_likelihood(double** data, unsigned int n)
{
  double ll = 0.0;
  double raw_log;

#pragma omp parallel for reduction(+:ll) private(raw_log)
  for (size_t k = 0; k < n; ++k)
  {
    raw_log = log_prob(data[k]);
    ll = ll + raw_log;
  }

  return ll;
}

double GMM::likelihood(double** data, unsigned int n)
{
  return exp(log_likelihood(data, n));
}

double GMM::log_prob(double* data)
{
  double p = 0.0;
  double somme = -DBL_MAX;

  for (unsigned int i = 0; i < nb_gauss; ++i)
  {
    p = gaussians[i].log_prob(data) + log(w[i]);
    somme = logAdd(somme, p );
  }
  return somme;
}

void GMM::expectation(double** data, unsigned int n)
{

  free_prob();
  p = new double*[nb_gauss];
  for (unsigned int k = 0; k < nb_gauss; ++k)
    p[k] = new double[n];

  //std::cout << "Expectation: " << log_likelihood(data, n)
  //          <<  std::endl;
  int nthreads = 1, tid;
#pragma omp parallel private(tid)
  {
    tid = omp_get_thread_num(); 
    if (tid == 0)
      {
	nthreads = omp_get_num_threads();
       	std::cout << "Number of threads = "
		  << nthreads << std::endl;
      }
  }
  double pwo[nthreads][nb_gauss];

#pragma omp parallel for
  for (int id = 0; id < nthreads; ++id)
  {
#pragma omp critical
    {
    for (unsigned int gauss = 0; gauss < nb_gauss; ++gauss)
      pwo[id][gauss] = w[gauss];
    }
  }
  for (unsigned int gauss = 0; gauss < nb_gauss; ++gauss)
    {
#pragma omp parallel for
      for (unsigned int t = 0; t < n; ++t)
	{
	  double pgausstlog = log(pwo[omp_get_thread_num()][gauss]) + gaussians[gauss].log_prob(data[t]);
	  double den = -DBL_MAX;
	  
	  for (unsigned int k = 0; k < nb_gauss; ++k)
	    den = logAdd(den, log(pwo[omp_get_thread_num()][k]) + gaussians[k].log_prob(data[t]));

	  pgausstlog -= den;
	  p[gauss][t] = pgausstlog;
	}
    }
}
