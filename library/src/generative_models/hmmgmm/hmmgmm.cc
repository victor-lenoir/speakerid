#include "hmmgmm.hh"
#include <iostream>

HMMGMM::HMMGMM (double** transition_prob_,
                size_t size_,
                std::vector<GMM*> gaussians_)
  : HMM<float*>(transition_prob_, size_),
    gaussians (gaussians_)
{
}

double HMMGMM::get_emission_prob (size_t state,
                                  double* obs)
{
  return gaussians[state]->prob(obs);
}
