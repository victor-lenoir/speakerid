#ifndef HMMGMM_HH_
# define HMMGMM_HH_

# include <vector>
# include "../hmm/hmm.hh"
# include "../gmm/gmm.hh"

class HMMGMM : public HMM<float*>
{
public:
  HMMGMM (double** transition_prob_,
	  size_t size_,
	  std::vector<GMM*> gaussians_);

  virtual double get_emission_prob (size_t state,
				    double* obs);

  std::vector<GMM*> gaussians;
};

#endif // !HMMGMM_HH_
