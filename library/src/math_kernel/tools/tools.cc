#include "tools.hh"
#include <cblas.h>
#include "math_kernel/lapacke/lapacke.h"
#include <iostream>
#include <cstdio>

void print_matrix(int m, int n, double* a)
{
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
      printf("%6.4f\t", a[i+j*m]);
    printf("\n");
  }
}

double det_sym(double* mat,
               int dim)
{
  double matrix[dim * dim];
  char c = 'U';
  int lda = dim;
  int info = 0;
  double det = 1.0;

  for (int y = 0; y < dim; ++y)
    for (int x = 0; x < dim; ++x)
      matrix[x + y * dim] = mat[x + y * dim];
  dpotrf_(&c, &dim, matrix, &lda, &info);

  for (int k = 0; k < dim; ++k)
    det *= matrix[k + k * dim];

  return (det * det);
}

bool inv_sym(double* matrix,
	     int dim,
	     double* invmat)
{
  char uplo = 'U';
  int info = 0;

  for (int k = 0;
       k < dim * dim;
       ++k)
    invmat[k] = matrix[k];
  dpotri_(&uplo, &dim, invmat, &dim, &info);
  return true;
}

bool eigs(double* matrix,
          int dim,
          double** eigval,
          double** eigvec)
{
  int n = dim;
  int lda = dim;
  int info;
  int lwork;
  double wkopt;
  double* work;
  char ulo = 'U';
  char jobz = 'V';

  (*eigval) = new double[dim];
  (*eigvec) = new double[dim * dim];
  for (int y = 0; y < dim; ++y)
    for (int x = 0; x < dim; ++x)
      (*eigvec)[x + y * dim] = matrix[x + y * dim];

  lwork = -1;
  dsyev_(&jobz, &ulo, &n, *eigvec, &lda, *eigval, &wkopt, &lwork, &info );
  lwork = (int)wkopt;
  work = new double[lwork];
  dsyev_(&jobz, &ulo, &n, *eigvec, &lda, *eigval, work, &lwork, &info );
  if (info > 0)
  {
    printf("eigs: The algorithm failed to compute eigenvalues for %d\n", info);
    return false;
  }

  delete[] work;

  return true;
}
