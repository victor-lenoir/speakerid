#ifndef TOOLS_HH_
# define TOOLS_HH_

# define UMAT(i, j) (i + j * ( j + 1 ) / 2)

bool eigs(double* matrix,
          int dim,
          double** eigval,
          double** eigvec);

double det_sym(double* cov_inv,
	       int dim);

bool inv_sym(double* matrix,
	     int dim,
	     double* invmat);

void print_matrix(int m, int n, double* a);

#endif // !TOOLS_HH_
