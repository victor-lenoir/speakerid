#ifndef MATRIX_HH_
# define MATRIX_HH_

# include <cstdlib>
# include <iostream>
# include <map>

class Matrix
{
public:
  Matrix(size_t nrow_, size_t ncol_);
  Matrix(const Matrix& a);

  ~Matrix();

  Matrix& operator= (const Matrix& rhs);
  Matrix& operator= (double rhs);

  Matrix operator+(const Matrix & m);
  Matrix& operator+=(const Matrix & m);

  Matrix operator-(const Matrix & m);
  Matrix& operator-=(const Matrix & m);

  Matrix operator*(const Matrix & m);

  Matrix& operator[](size_t index);
  Matrix operator*(double scalar);
  Matrix& operator*=(double scalar);

  Matrix operator/(double scalar);
  Matrix& operator/=(double scalar);

  Matrix operator/(const Matrix & m);

  Matrix transpose();

  Matrix row(size_t index);
  void set_to_identity();

  size_t nrow;
  size_t ncol;

  double* m;
private:
  bool sub_matrix;
  std::map<size_t, Matrix*> sub_matrices;
  void clean();
  void allocate();
};

std::ostream& operator<< (std::ostream& stream,
                          const Matrix& matrix);
#endif // !MATRIX_HH_
