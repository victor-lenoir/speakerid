#include <cstring>
#include <cblas.h>
#include "matrix.hh"

Matrix::Matrix(size_t nrow_, size_t ncol_)
  : nrow(nrow_),
    ncol(ncol_)
{
  sub_matrix = false;
  m = 0;
  allocate();
}

Matrix::Matrix(const Matrix& a)
{
  sub_matrix = false;
  nrow = a.nrow;
  ncol = a.ncol;
  allocate();

  memcpy(m, a.m, sizeof(double) * ncol * nrow);
}

Matrix::~Matrix()
{
  clean();
}

void Matrix::allocate()
{
  m = new double[nrow * ncol];
  for (size_t i = 0; i < nrow * ncol; ++i)
    m[i] = 0.0;
}

void Matrix::clean()
{
  if ((m != 0) && (!sub_matrix))
  {
    delete[] m;
    m = 0;
  }
  for (std::map<size_t, Matrix*>::iterator it = sub_matrices.begin();
       it != sub_matrices.end();
       it++)
  {
    delete it->second;
  }
}


Matrix Matrix::row(size_t index)
{
  Matrix ret(ncol, 1);

  memcpy(ret.m, m + ncol * index,sizeof(double) * ncol);
  return ret;
}

Matrix& Matrix::operator= (const Matrix& a)
{
  if (this != &a)
  {
    if ((nrow != a.nrow) || (ncol != a.ncol))
    {
      clean();
      nrow = a.nrow;
      ncol = a.ncol;
      allocate();
    }
    memcpy(m, a.m, sizeof(double) * ncol * nrow);
  }
  return *this;
}

void Matrix::set_to_identity()
{
  if (nrow != ncol)
    std::cerr << "Only square matrix allowed" << std::endl;
  else
    for (size_t i = 0; i < nrow; ++i)
      m[i + i * nrow] = 1.0;
}

Matrix& Matrix::operator+=(const Matrix& a)
{
  if ((nrow != a.nrow) || (ncol != a.ncol))
    std::cerr << "Dimensions not compatible" << std::endl;
  else
    for (size_t i = 0; i < nrow * ncol; ++i)
      m[i] += a.m[i];
  return *this;
}

Matrix Matrix::operator+(const Matrix& m)
{
  Matrix temp(*this);

  return (temp += m);
}

Matrix& Matrix::operator-=(const Matrix& a)
{
  if ((nrow != a.nrow) || (ncol != a.ncol))
    std::cerr << "Dimensions not compatible" << std::endl;
  else
    for (size_t i = 0; i < nrow * ncol; ++i)
      m[i] -= a.m[i];
  return *this;
}


Matrix Matrix::operator-(const Matrix& m)
{
  Matrix temp(*this);

  return (temp -= m);
}

Matrix& Matrix::operator[](size_t index)
{
  if (sub_matrices.count(index))
    return *sub_matrices[index];
  else if ((nrow == 1) && (index < ncol))
  {
    Matrix* ret = new Matrix(1, 1);

    ret->m = m + index;
    ret->sub_matrix = true;
    sub_matrices.insert(std::pair<size_t, Matrix*>(index, ret));
    return (*ret);
  }
  else if (index < nrow)
  {
    Matrix* ret = new Matrix(1, ncol);

    ret->sub_matrix = true;
    ret->m = m + ncol * index;
    sub_matrices.insert(std::pair<size_t, Matrix*>(index, ret));
    return (*ret);
  }
  else
  {
    std::cerr << "operator[]: Index out of bound on matrix:"
              << std::endl << (*this) << "Index: "
              << index << std::endl;
    return *(new Matrix(1, 1));
  }

}
Matrix& Matrix::operator= (double rhs)
{
  if ((nrow == 1) && (ncol == 1))
    m[0] = rhs;
  else
    std::cerr << "Can't affect a double to a matrix" << std::endl;
  return (*this);
}

Matrix Matrix::operator*(const Matrix& a)
{
  Matrix content(nrow, a.ncol);

  if ((nrow == 1) && (ncol == 1))
  {
    Matrix ret(a);

    ret *= m[0];
    return ret;
  }
  else if ((a.nrow == 1) && (a.ncol == 1))
  {
    Matrix ret((*this));

    ret *= a.m[0];
    return ret;
  }
  else if (ncol != a.nrow)
  {
    std::cerr << "Mul: dimensions incompatible" << std::endl;
  }
  else
    cblas_dgemm(CblasColMajor,
                CblasNoTrans,
                CblasNoTrans,
                nrow, a.ncol, ncol, 1.0, m,
                nrow, a.m, a.nrow,
                1.0, content.m,
                content.nrow);
  return content;
}

Matrix Matrix::operator/(const Matrix& a)
{
  Matrix con(1 ,1);

  if ((nrow == 1) && (ncol == 1))
  {
    Matrix ret(a);

    ret /= m[0];
    return ret;
  }
  else if ((a.nrow == 1) && (a.ncol == 1))
  {
    Matrix ret((*this));

    ret /= a.m[0];
    return ret;
  }
  else
    std::cerr << "Div error: inverse the matrice then multiply"
              << std::endl;

  return con;
}

Matrix& Matrix::operator*=(double scalar)
{
  for (size_t i = 0; i < nrow * ncol; ++i)
    m[i] *= scalar;
  return *this;
}

Matrix Matrix::operator/(double scalar)
{
  Matrix temp(*this);

  return (temp /= scalar);
}

Matrix& Matrix::operator/=(double scalar)
{
  for (size_t i = 0; i < nrow * ncol; ++i)
    m[i] /= scalar;
  return *this;
}

Matrix Matrix::operator*(double scalar)
{
  Matrix temp(*this);

  return (temp *= scalar);
}

Matrix Matrix::transpose()
{
  Matrix tmp(ncol, nrow);

  for (size_t i = 0; i < nrow; ++i)
    for (size_t j = 0; j < ncol; ++j)
      tmp.m[j + i * ncol] = m[i + j * nrow];
  return tmp;
}

std::ostream& operator<< (std::ostream& s,
                          const Matrix& matrix)
{
  for (size_t i = 0; i < matrix.nrow; ++i)
  {
    for (size_t j = 0; j < matrix.ncol; ++j)
      s << matrix.m[i + j * matrix.nrow] << " ";
    s << std::endl;
  }
  s << "Dim: " << matrix.nrow
    << "x" << matrix.ncol << std::endl;
  return s;
}
