#include <ctime>
#include <cstdlib>
#include <cmath>

#include "sda.hh"

SDA::SDA(size_t dimdest_)
{
  w = 0;
  w_trans = 0;
  dimdest = dimdest_;
  grad_step = 5.0;
}

SDA::SDA(size_t dimdest_, double grad_step_)
  : grad_step(grad_step_)
{
  dimdest = dimdest_;
  w = 0;
  w_trans = 0;
}

SDA::SDA(size_t dimdest_, double grad_step_, Matrix* w_)
  : grad_step(grad_step_),
    w(w_)
{
  w_trans = 0;
  dimdest = dimdest_;
}

SDA::~SDA()
{
  if (w)
    delete w;
  if (s)
    delete s;
}

Matrix SDA::dj(std::vector<Matrix>& xs)
{
  Matrix ret(w->nrow, w->ncol);

  if (w_trans)
    delete w_trans;
  w_trans = new Matrix(w->transpose());

  for (size_t k = 0; k < c; ++k)
    for (size_t k2 = k; k2 < c; ++k2)
    {
      for (size_t i = 0; i < xs[k].nrow; ++i)
        for (size_t j = i + 1; j < xs[k2].nrow; ++j)
        {
	  std::cout << i << " " << j << std::endl;
          Matrix xi(xs[k].row(i));
          Matrix xit(xi.transpose());
          Matrix xj(xs[k2].row(j));
          Matrix xjt(xj.transpose());
          double fi = sqrt((xit * (*w) * (*w_trans) * xi).m[0]);
          double fj = sqrt((xjt * (*w) * (*w_trans) * xj).m[0]);
          double fij = (xit * (*w) * (*w_trans) * xj).m[0];
          double wij = ((*s)[k][k2]).m[0]; // or -wij, not sure

          ret += (xi * xit * (*w) * fij) / (pow(fi, 3.0) * fj) * wij;
          ret += (xj * xjt * (*w) * fij) / (pow(fj, 3.0) * fi) * wij;
          ret -= ((xi * xjt * (*w) + xj * xit * (*w)) /
                  (fi * fj)) * wij;
        }
    }

  ret *= 2.0;
  return ret;
}

double SDA::j(std::vector<Matrix>& xs)
{
  double ret = 0.0;
  double tmp;

  if (w_trans)
    delete w_trans;
  w_trans = new Matrix(w->transpose());

  for (size_t k = 0; k < c; ++k)
    for (size_t k2 = k; k2 < c; ++k2)
    {
      for (size_t i = 0; i < xs[k].nrow; ++i)
        for (size_t j = i + 1; j < xs[k2].nrow; ++j)
        {
          Matrix mat((*w_trans) * xs[k].row(i));
          Matrix mat2((*w_trans)* xs[k2].row(j));

          mat = mat / (mat.transpose() * mat);
          mat2 = mat2 / (mat2.transpose() * mat2);

          mat = mat - mat2;

          tmp = (mat * mat.transpose()).m[0];
          ret += tmp * tmp * (*s)[k][k2].m[0];
        }
    }

  return ret;
}

void SDA::compute_w(std::vector<Matrix>& xs)
{
  if (xs.size() == 0)
    return;
  c = xs.size();
  w = new Matrix(xs[0].ncol, dimdest);

  srand(time(NULL));
  std::cout << "Randomly initializing w" << std::endl;
  for (size_t i = 0; i < w->ncol * w->nrow; ++i)
    w->m[i] = (rand() / (double)RAND_MAX) * 10.0;

  std::cout << "Done." << std::endl;
  s = new Matrix(c, c);
  std::cout << "Randomly initializing w" << std::endl; 
  for (size_t i = 0; i < c; ++i)
    for (size_t j = 0; j < c; ++j)
      if (i == j)
        s->m[i + j * c] = 1.0 / (double)(c * xs[i].nrow * xs[i].nrow);
      else
        s->m[i + j * c] = 1.0 / (double)(c * (c - 1) * xs[i].nrow *
                                         xs[j].nrow);
  //std::cout << "S = " << std::endl << (*s) << std::endl;
  //std::cout << "c = " << c << std::endl;
  std::cout << "Done." << std::endl;
  std::cout << "Computing j" << std::endl;
  double before = j(xs);
  //double last = before;
  size_t iter = 0;
  std::cout << "Done." << std::endl;
  while (1)
  {
    std::cout << "iteration " << iter << std::endl;
    (*w) = (*w) - (dj(xs) * grad_step);
    //double jj = j(xs);

    //std::cout << " => " << jj - last << std::endl;
    //if (fabs(jj - last) < CONVERGENCE)
    //{
    //  std::cout << "CONVERGENCE" << std::endl;
    //  break;
    //}
    //last = jj;
    grad_step *= 0.99;
    ++iter;
    break;
  }
  double after = j(xs);
  std::cout << (*w) << std::endl;
  std::cout << "J_BEFORE = " << before << std::endl;
  std::cout << "J_AFTER = " << after << std::endl;

  if (after < before)
    std::cout << "WIN" << std::endl;
  else
    std::cout << "LOSE" << std::endl;
}

void SDA::project(std::vector<double>& x)
{
  (void)x;
}

void SDA::project(std::vector<std::vector<double> >& xs)
{
  (void)xs;
}
