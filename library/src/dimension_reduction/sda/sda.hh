#ifndef SDA_HH_
# define SDA_HH_

# include <vector>
# include "math_kernel/matrix/matrix.hh"

# define CONVERGENCE 10e-12

class SDA
{
public:
  SDA(size_t dimdest_);
  SDA(size_t dimdest_, double grad_step_);
  SDA(size_t dimdest_, double grad_step_, Matrix* w_);

  ~SDA();

  double j(std::vector<Matrix>& classes);
  Matrix dj(std::vector<Matrix>& classes);
  void compute_w(std::vector<Matrix>& classes);
  void project(std::vector<double>& x);
  void project(std::vector<std::vector<double> >& xs);

  size_t c;
  size_t dimdest;
  double grad_step;
  Matrix* w;
  Matrix* w_trans;
  Matrix* s;
};

#endif // !SDA_HH_
