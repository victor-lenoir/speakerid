#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <list>
#include "sound.hh"
#include "formula_parser.hh"
#include "fft.hh"

Sound::Sound(std::string filename)
{
  shift_size = 10;
  window_size = 25;
  if (!buff.loadFromFile(filename))
    {
      std::cerr << "Failed to load "
		<< filename << std::endl;
    }
  setBuffer(buff);
  freq = buff.getSampleRate();
  nb_sample = buff.getSampleCount();
  samples = buff.getSamples();
}

void Sound::plot_signal(sf::RenderWindow& window,
			int inten)
{
  sf::VertexArray lines(sf::LinesStrip, nb_sample);
  const sf::Int16* samples = buff.getSamples();
  unsigned int max_sample = 0;

  for (unsigned int k = 0; k < nb_sample; ++k)
    if (fabs(samples[k]) > max_sample)
      max_sample = fabs(samples[k]);
  for (unsigned int k = 0; k < nb_sample; ++k)
    {
      lines[k].color = sf::Color(inten, inten, inten);
      lines[k].position = sf::Vector2f((800 * k) / (float)(nb_sample),
				       300 - ((300.0 * samples[k])
					      / max_sample));
    }
  window.draw(lines);
}

void Sound::base_draw(sf::RenderWindow& window,
		      sf::VertexArray* features)
{
  bool ispause = true;
  while (window.isOpen())
    {
      sf::Event event;
      sf::RectangleShape rect;

      rect.setSize(sf::Vector2f(2,600));
      rect.setFillColor(sf::Color(255, 0, 0));
      rect.setPosition(800 * (double)getPlayingOffset().asMilliseconds() /
		       (double)buff.getDuration().asMilliseconds(), 0);
      window.clear();
      plot_signal(window, 255);
      if (features)
	window.draw(*features);
      window.draw(rect);
      window.display();
      while (window.pollEvent(event))
	{
	  if (event.type == sf::Event::Closed)
	    window.close();
	  else if (event.type == sf::Event::MouseButtonPressed)
	    {
	      ispause = !ispause;
	      if (ispause)
		pause();
	      else
		play();
	    }
	}
    }
}

void Sound::plot_feature (std::string formula)
{
  std::list<double> feature = get_feature (formula);
  sf::RenderWindow window(sf::VideoMode(800, 600), "Feature plot");
  sf::VertexArray featureline(sf::LinesStrip, feature.size());
  unsigned int j = 0;
  double max_feature = 0;

  for (std::list<double>::iterator it = feature.begin ();
       it != feature.end ();
       ++it)
    if (fabs((*it)) > max_feature)
      max_feature = fabs((*it));
  for (std::list<double>::iterator it = feature.begin ();
       it != feature.end ();
       ++it)
  {
    featureline[j].color = sf::Color(255, (255.0 * (*it)) / max_feature,
				     0);
    featureline[j].position = sf::Vector2f((800 * j) / (float)(feature.size()), 300 - ((300.0 * (*it)) / max_feature));
    ++j;
  }
  window.draw(featureline);
  base_draw(window, &featureline);
}

void Sound::plot()
{
  sf::RenderWindow window(sf::VideoMode(800, 600), "Sound plot");

  base_draw(window, 0);
}

std::list<double> Sound::get_signal_flatness ()
{
  if (signal_flatness.size () > 0)
    return signal_flatness;
  else
  {
    int frame_shift = shift_size * freq / 1000;
    int frame_window = window_size * freq / 1000;
    int i = 0;

    for (unsigned int xwind = 0; xwind < nb_sample; xwind += frame_shift)
    {
      double mean = 0.0;
      double sum = 0.0;
      i = 0;
      for (unsigned int xdefil = xwind; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
      {
        sum += samples[xdefil];
        ++i;
      }
      mean = sum / i;
      sum = 0.0;
      for (unsigned int xdefil = xwind; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
      {
        sum += (samples[xdefil] - mean) * (samples[xdefil] - mean);
      }
      sum /= i;
      sum = sqrt (sum);
      signal_flatness.push_back (1.0 / (0.0000001 + sum));
      ++i;
    }
    return signal_flatness;
  }
}

std::list<double> Sound::get_spectral_flatness ()
{
  if (sfm.size () > 0)
    return sfm;
  else
  {
    std::vector<std::vector<double> > spec = get_spectrogram ();

    for (unsigned int i = 0; i < spec.size (); ++i)
    {
      double mean = 0.0;
      double sum = 0.0;
      int n = 0;

      for (unsigned int u = 0; u < spec[i].size (); ++u)
      {
        sum += spec[i][u];
        ++n;
      }

      mean = sum / n;
      sum = 0.0;

      for (unsigned int u = 0; u < spec[i].size (); ++u)
      {
        sum += (spec[i][u] - mean) * (spec[i][u] - mean);
      }
      sum /= n;
      sum = sqrt (sum);
      sfm.push_back (1.0 / (0.0000001 + sum));

    }
    return sfm;
  }
}

std::list<long>       Sound::get_max_freq_index ()
{
  if (max_freq_index.size () > 0)
    return max_freq_index;
  else
  {
    std::vector<std::vector<double> > spec = get_spectrogram ();

    for (unsigned int i = 0; i < spec.size (); ++i)
    {
      long maxindex = 0;
      double max = 0.0;

      for (unsigned int u = 0; u < spec[i].size (); ++u)
      {
        if (max < spec[i][u])
        {
          max = spec[i][u];
          maxindex = u;
        }
      }
      max_freq_index.push_back (maxindex);

    }
    return max_freq_index;
  }
}

std::list<double> Sound::get_max_freq_ampl ()
{
  if (max_freq_ampl.size () > 0)
    return max_freq_ampl;
  else
  {
    std::vector<std::vector<double> > spec = get_spectrogram ();

    for (unsigned int i = 0; i < spec.size (); ++i)
    {
      double max = 0.0;

      for (unsigned int u = 0; u < spec[i].size (); ++u)
      {
        if (max < spec[i][u])
          max = spec[i][u];
      }
      max_freq_ampl.push_back (max);

    }
    return max_freq_ampl;
  }
}

std::vector<std::vector<double> > Sound::get_spectrogram ()
{
  if (spectrogram.size () > 0)
    return spectrogram;
  else
  {
    int frame_shift = shift_size * freq / 1000;
    int frame_window = window_size * freq / 1000;
    int i = 0;

    for (unsigned int xwind = 0; xwind < nb_sample; xwind += frame_shift)
    {
      std::vector<double> toadd;

      i = 0;
      for (unsigned int xdefil = xwind; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
        ++i;

      int n = i;
      int k = log2 (n);
      int i;
      double* re = (double*)malloc (sizeof(double) * n);
      double* im = (double*)malloc (sizeof(double) * n);
      i = 0;
      for (unsigned int xdefil = xwind; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
      {
        im[i] = 0.0;
        re[i] = samples[xdefil];
        ++i;
      }

      fft(re,im,k,+1);

      for(i = 0; i < n / 2; i++)
        toadd.push_back (sqrt (re[i] * re[i] + im[i] * im[i]));

      free (re);
      free (im);

      spectrogram.push_back (toadd);
    }
    return spectrogram;
  }
}
std::list<double>     Sound::get_spectrum ()
{
  if (spectrum.size () > 0)
    return spectrum;
  else
  {
    int n = nb_sample;
    int k = log2 (n);
    int i;
    double* re = (double*)malloc (sizeof(double) * n);
    double* im = (double*)malloc (sizeof(double) * n);

    for(i = 0; i < n; ++i)
    {
      re[i] = samples[i];
      im[i] = 0;
    }

    fft(re,im,k,+1);

    for(i = 0; i < n / 2; i++)
      spectrum.push_back (sqrt (re[i] * re[i] + im[i] * im[i]));

    free (re);
    free (im);
    return spectrum;
  }
}
std::list<double>       Sound::get_feature(std::string   formula)
{
  FormulaParser         fp (formula, this);

  return fp.get_result ();
}

std::list<double> Sound::get_energy ()
{
  if (energy.size () > 0)
    return energy;
  else
  {
    int frame_shift = shift_size * freq / 1000;
    int frame_window = window_size * freq / 1000;
    int i = 0;

    for (unsigned int xwind = 0; xwind < nb_sample; xwind += frame_shift)
    {
      double tmpenergy = 0.0;
      if (xwind + frame_window < nb_sample)
      {
        for (unsigned int xdefil = xwind; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
          tmpenergy += samples[xdefil] * samples[xdefil];
        tmpenergy = sqrt(tmpenergy);
        energy.push_back (tmpenergy);
        ++i;
      }
    }
    return energy;
  }
}

std::list<long> Sound::get_zero_crossing ()
{
  if (zero_cross.size () > 0)
  {
    return zero_cross;
  }
  else
  {
    int frame_shift = shift_size * freq / 1000;
    int frame_window = window_size * freq / 1000;
    int i = 0;

    for (unsigned int xwind = 0; xwind < nb_sample; xwind += frame_shift)
    {
      long tmpzero = 0;
      if (xwind + frame_window < nb_sample)
      {
        for (unsigned int xdefil = xwind + 1; (xdefil < nb_sample) && (xdefil < xwind + frame_window); ++xdefil)
        {
          if (samples[xdefil] * samples[xdefil - 1] < 0)
            ++tmpzero;
        }
        zero_cross.push_back(tmpzero);
      }
      ++i;
    }
    return zero_cross;
  }
}
