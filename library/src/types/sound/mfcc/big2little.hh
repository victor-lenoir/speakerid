#ifndef BIG2LITTLE_HH
# define BIG2LITTLE_HH


inline void swap(char &a, char &b)
{
  char tmp = a;
  a = b;
  b = tmp;
}

inline void big2littleendian(char *in, unsigned nboctet)
{
  switch (nboctet)
  {
  case 8:
    swap(in[0], in[7]);
    swap(in[1], in[6]);
    swap(in[2], in[5]);
    swap(in[3], in[4]);
    break;
  case 4 :
    swap(in[0], in[3]);
    swap(in[1], in[2]);
    break;
  case 2 :
    swap(in[0], in[1]);
    break;
  }
}

inline void big2littleendian(float &in)
{
  big2littleendian((char *) (&in), 4);
}

inline void big2littleendian(double &in)
{
  big2littleendian((char *) (&in), 8);
}

#endif // !BIG2LITTLE_HH
