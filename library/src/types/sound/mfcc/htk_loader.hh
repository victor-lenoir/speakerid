#ifndef HTK_LOADER_HH
# define HTK_LOADER_HH

# include <string>

# include "loader.hh"

class HtkLoader : public Loader
{
public:
  HtkLoader (bool end):Loader(end) { }
  ~HtkLoader () { }

  /*!
  ** Loads an htk file.
  */
  float** load(const std::string& path, int& size);

  /*!
  ** Returns the number of features in a htk file.
  */
  unsigned long long int nb_feature(const std::string& path);

  /*!
  ** Returns the size of the features in a htk file.
  */
  int feature_size(const std::string& path);
};


#endif /* !HTK_LOADER_HH */
