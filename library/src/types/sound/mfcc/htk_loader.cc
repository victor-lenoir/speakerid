#include <sys/types.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "htk_loader.hh"
#include "big2little.hh"

float**
HtkLoader::load(const std::string& path,
		int&	size)
{
  std::ifstream input_file;
  unsigned int nSamples;	/*- number of samples in file (4-byte integer)*/
  unsigned int sampPeriod;     /*- sample period in 100ns units (4-byte integer)*/
  unsigned short sampSize;       /*- number of bytes per sample (2-byte integer)*/
  unsigned short parmKind;      /*- a code indicating the sample kind (2-byte integer) */
  float** res;
  unsigned int dim;

  input_file.open (path.c_str (), std::ios::binary);
  if (!input_file)
    throw std::string ("Failed to open Htk file in htk_loader.cc : ") + path;

  input_file.read ((char*) &nSamples, 4);
  input_file.read ((char*) &sampPeriod, 4);
  input_file.read ((char*) &sampSize, 2);
  input_file.read ((char*) &parmKind, 2);

  if (bigEndian ())
    {
      big2littleendian((char*) &nSamples, 4);
      big2littleendian((char*) &sampPeriod, 4);
      big2littleendian((char*) &sampSize, 2);
      big2littleendian((char*) &parmKind, 2);
    }

  dim = (unsigned int) (sampSize/(sizeof(float)));

  try {
    res = new float*[nSamples];
  } catch (...) {
    exit (2);
  }

  for (unsigned i = 0; i < nSamples; ++i)
    try {
      res[i]= new float[dim];
    } catch (...) {
      exit (2);
    }

  size = nSamples;

  if (bigEndian ())
    {
      for (unsigned int i = 0; i < nSamples; ++i)
	{
	  input_file.read ((char*) res[i], sizeof (float) * dim);
	  for(unsigned int j = 0; j < dim; ++j)
	    big2littleendian(res[i][j]);
	}
    }
  else
    for (unsigned int i = 0; i < nSamples; ++i)
      input_file.read ((char*) res[i], sizeof (float) * dim);

  input_file.close ();
  return res;
}

unsigned long long
HtkLoader::nb_feature(const std::string& path)
{
  std::ifstream input_file;
  //int32_t nSamples;	/*- number of samples in file (4-byte integer)*/
  unsigned int nSamples;

  input_file.open (path.c_str (), std::ios::binary);
  if (!input_file)
    throw std::string ("Failed to open Htk file in htk_loader.cc : ") + path;
  input_file.read ((char*) &nSamples, 4);
  if (bigEndian ())
    big2littleendian((char*) &nSamples, 4);
  input_file.close ();
  return nSamples;
}


int
HtkLoader::feature_size(const std::string& path)
{
 std::ifstream input_file;
 unsigned int nSamples;	/*- number of samples in file (4-byte integer)*/
 unsigned short sampSize;       /*- number of bytes per sample (2-byte integer)*/

 input_file.open (path.c_str (), std::ios::binary);
 if (!input_file)
   throw std::string (path + " : Failed to open Htk file in htk_loader.cc");

 input_file.read ((char*) &nSamples, 4);
 input_file.read ((char*) &nSamples, 4);
 input_file.read ((char*) &sampSize, 2);
 if(bigEndian ())
   big2littleendian((char*) &sampSize, 2);
 input_file.close ();
 return sampSize / sizeof (float);
}
