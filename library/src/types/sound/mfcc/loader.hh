#ifndef LOADER_HH
# define LOADER_HH

# include <string>

class Loader
{
public:
  Loader(bool end):bigEndian_(end) { };
  virtual ~Loader() { };

  /*!
  ** @param path The feature file to load.
  ** @param size A var in which will be stored the number of feature vectors loaded.
  **
  ** @return A float tab containing all the loaded feature vectors.
  */
  virtual float** load(const std::string& path, int& size) = 0;

  /*!
  ** @param path The feature file to process.
  **
  ** @return The number of features in path.
  */
  virtual unsigned long long nb_feature(const std::string& path) = 0;

  /*!
  ** @param path The feature file to process.
  **
  ** @return The size of features in path.
  */
  virtual int feature_size(const std::string& path) = 0;

  bool bigEndian() const { return bigEndian_; }
private:
  bool bigEndian_;
};


#endif /* !LOADER_HH */
