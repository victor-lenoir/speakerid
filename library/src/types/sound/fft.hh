#ifndef FFT_HH_
# define FFT_HH_

void fft(double *reel, double *imag, int log2n, int sign);

#endif /* FFT_HH_ */
