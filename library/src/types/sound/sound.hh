#ifndef SOUND_HH_
# define SOUND_HH_
# include <string>
# include "spectrogram.hh"
# include <list>
# include <vector>
# include <cstdlib>
# include <utility>
# include <SFML/Audio.hpp>
# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>

class Sound : public sf::Sound
{
public:
  Sound(std::string file);
  std::list<double>     get_energy ();
  int			get_nb_frame ()
  {
    return get_energy().size ();
  }
  void			load_sample (std::string	file);
  std::vector<std::vector<double> > get_spectrogram ();
  std::list<double>	get_signal_flatness ();
  std::list<double>	get_spectrum ();
  std::list<double>	get_max_freq_ampl ();
  std::list<double>	get_spectral_flatness ();
  std::list<long>	get_max_freq_index ();
  std::list<long>	get_zero_crossing ();
  void			plot_feature (std::string	formula);
  void			plot ();
  std::list<double>	get_feature (std::string	formula);

  sf::SoundBuffer buff;
  unsigned int shift_size;
  unsigned int window_size;
  unsigned int freq;
  unsigned int nb_sample;
  const sf::Int16* samples;


private:
  void plot_signal(sf::RenderWindow& window,
		   int inten);
  void base_draw(sf::RenderWindow& window,
		 sf::VertexArray* features);

  std::vector<std::vector<double> >	spectrogram;
  std::list<double>	spectrum;
  std::list<double>	energy;
  std::list<double>	max_freq_ampl;
  std::list<long>	max_freq_index;
  std::list<double>	sfm;
  std::list<long>	zero_cross;
  std::list<double>	signal_flatness;
};

#endif /* !SOUND_HH_ */
