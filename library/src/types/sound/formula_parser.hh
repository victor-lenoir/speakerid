#ifndef FORMULA_PARSER_HH_
# define FORMULA_PARSER_HH_
# include "sound.hh"
# include <ctype.h>

std::list<double> operator* (std::list<double> a,
			     std::list<double> b)
{
  std::list<double> result;
  std::list<double>::iterator	it;
  std::list<double>::iterator   it2;

  result.clear ();

  it2 = b.begin ();
  for (it = a.begin (); it != a.end (); ++it)
    {
      result.push_back ((*it) * (*it2));
      ++it2;
    }
  return result;
}

std::list<double> operator/ (std::list<double> a,
                             std::list<double> b)
{
  std::list<double> result;
  std::list<double>::iterator   it;
  std::list<double>::iterator   it2;

  result.clear ();

  it2 = b.begin ();
  for (it = a.begin (); it != a.end (); ++it)
    {
      result.push_back ((*it) / (*it2));
      ++it2;
    }
  return result;
}

void normalization (std::list<double>&	a)
{
  double max = 0.0;
  std::list<double>::iterator   it;

  for (it = a.begin (); it != a.end (); ++it) 
    {
      if (max < fabs (*it))
	max = fabs (*it);
    }
  if (max > 0)
    for (it = a.begin (); it != a.end (); ++it)
      (*it) /= max;
}
std::list<double> operator+ (std::list<double> a,
                             std::list<double> b)
{
  std::list<double> result;
  std::list<double>::iterator   it;
  std::list<double>::iterator   it2;

  result.clear ();

  it2 = b.begin ();
  for (it = a.begin (); it != a.end (); ++it)
    {
      result.push_back ((*it) + (*it2));
      ++it2;
    }
  return result;
}

std::list<double> operator- (std::list<double> a,
                             std::list<double> b)
{
  std::list<double> result;
  std::list<double>::iterator   it;
  std::list<double>::iterator   it2;

  result.clear ();

  it2 = b.begin ();
  for (it = a.begin (); it != a.end (); ++it)
    {
      result.push_back ((*it) - (*it2));
      ++it2;
    }
  return result;
}

class FormulaParser
{
public:
  FormulaParser (std::string	formula_,
		 Sound*		s_)
  {
    s = s_;
    formula = formula_;
    index = 0;
    result.clear ();
    result = parse_first ();
  }

private:
  // or not
  std::string toupper (std::string e)
  {
    std::string ret = "";

    for(unsigned int i = 0; i < e.size(); ++i)
      {
	std::string a = "a";

	a[0] = tolower(e[i]);
	if (((a[0] >= 'a') && (a[0] <= 'z')) ||
	    ((a[0] >= '0') && (a[0] <= '9')))
	  ret = ret + a;
      }

    return ret;
  }
  std::list<double> list_long_to_double (std::list<long> l)
  {
    std::list<long>::iterator     it;
    std::list<double> res;

    for (it = l.begin (); it != l.end (); ++it)
      {
	res.push_back (*it);
      }
    return res;
  }
  inline bool is_digit (char c)
  {
    return ((c >= '0') && (c <= '9'));
  }

  inline bool is_op (char c)
  {
    return ((c == '+') || (c == '-') || (c == '*') || (c == '/'));
  }
  inline bool is_blank (char c)
  {
    return ((c == ' ') || (c == '\t') || (c == '\n') || (c == '\r'));
  }

  std::list<double> list_from_int (std::string	n)
  {
    double value = atof (n.c_str ());
    std::list<double> l (s->get_energy().size (), value);

    return l;
  }
  std::list<double> parse_final ()
  {
    std::string	word = "";
    bool	normalize = false;
    std::list<double>       result;

    while ((index < formula.size ()) && (is_blank (formula[index])))
      ++index;
    while (formula[index] == '!')
     {
    	normalize = true;
    	++index;
     }
    if (formula[index] == '(')
      {
	std::string		newformula = "";
	int			par = 1;

	++index;
	while ((par > 0) && (index < formula.size ()))
	  {
	    if (formula[index] == ')')
	      --par;
	    else if (formula[index] == '(')
	      ++par;
	    if (par > 0)
	      {
		newformula = newformula + formula[index];
	      }
	    ++index;
	  }
	if (par > 0)
	  {
	    std::cerr << "FormulaParser: Missing ) parenthesis"
		      << std::endl;
	    exit (2);
	  }
	else
	  {
	    FormulaParser fp (newformula, s);

	    result = fp.get_result ();
	    // return fp.get_result ();
	  }
      }
    else
      {
	while ((index < formula.size ()) && (!is_op (formula[index])))
	  {
	    if (!is_blank(formula[index]))
	      {
		word = word + formula[index];
	      }
	    ++index;
	  }
	word = toupper (word);
	if ((word == "e") || (word == "energy"))
	  {
	    result = s->get_energy ();
	  }
	else if ((word == "signalflatness") || (word == "sf"))
	  result = s->get_signal_flatness ();
	else if ((word == "mfa") || (word == "maxfreqampl") || (word == "maxfreqamplitude") || (word == "maxfrequencyamplitude"))
	  result = s->get_max_freq_ampl ();
	else if ((word == "mfi") || (word == "maxfreqindex") || (word == "maxfrequencyindex"))
	  result = (list_long_to_double (s->get_max_freq_index ()));
	else if ((word == "sfm") || (word == "spectralflatnessmeasure") || (word == "sf") || (word == "spectralflatness"))
	  result = s->get_spectral_flatness ();
	else if ((word == "z") || (word == "tpz") || (word == "zerocrossing")
		 || (word == "zerocross"))
	  result = list_long_to_double (s->get_zero_crossing ());
	else if (isdigit (word[0]))
	  result = (list_from_int(word));
	else
	  {
	    std::cerr << "FormulaParser: Unrecognized feature: <" << word
		      << ">" << std::endl << std::endl;
	    std::cerr << "Features list (computed for each frame) :"
		      << std::endl << std::endl
		      << "Max_freq_amplitude[MFA]\tMax frequency amplitude\tFrequency domain"
		      << std::endl
		      << "Max_freq_index[MFI]\tMax frequency index\tFrequency domain"
                      << std::endl
		      << "Zero_cross[Z]\t\tZero crossing number\tTime domain"
		      << std::endl
		      << "Spectral_flatness[SFM]\tSpectral flatness\tFrequency domain"
		      << std::endl
		      << "Signal_flatness[SF]\tSignal Flatness\t\tTime domain"
                      << std::endl
		      << "Energy[E]\t\tEnergy of each frame\tTime domain"
		      << std::endl << std::endl;
	    std::cerr << "Example of formula: (Energy * SFM) / (Zero_cross)" << std::endl << std::endl; 
	    exit (2);
	  }
      }
    if (normalize)
      normalization (result);

    return result;
  }

  std::list<double> parse ()
  {
    std::list<double>	ret = parse_final ();

    while (index < formula.size ())
      {
	if (formula[index] == '*')
	  {
	    ++index;
	    ret = ret * parse_final ();
	  }
	else if (formula[index] == '/')
	  {
	    ++index;
	    ret = ret / parse_final ();
	  }
	else
	  break;
      }
    return ret;
  }

  std::list<double> parse_first ()
  {
    std::list<double>   ret = parse ();

    while (index < formula.size ())
      {
        if (formula[index] == '+')
          {
            ++index;
            ret = ret + parse ();
          }
        else if (formula[index] == '-')
          {
            ++index;
            ret = ret - parse ();
          }
        else
          {
	    /*
	    std::cerr << "FormulaParser: Operator " << formula[index]
                      << " unknown" << std::endl;
            exit (2);
	    */
	    break;
          }
      }
    return ret;
  }

public:
  std::list<double> get_result ()
  {
    return result;
  }
private:
  Sound*		s;
  unsigned int		index;
  std::string		formula;
  std::list<double>	result;
};
#endif /* FORMULA_PARSER_HH_ */
